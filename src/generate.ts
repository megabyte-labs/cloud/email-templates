import * as fs from 'fs';
import * as handlebars from 'handlebars';
import * as hbs from 'hbs';
import * as mjml2html from 'mjml';

import { InterestCategories } from './interest-categories';

const site = 'https://videoblobs.com';
const languages = ['es', 'fr', 'jp', 'zh'];
const translations = require('./translations.json');

const readFile = (path, opts = 'utf8') =>
    new Promise((resolve, reject) => {
        fs.readFile(path, opts, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });

const writeFile = (path, data, opts = 'utf8') =>
    new Promise((resolve, reject) => {
        fs.writeFile(path, data, opts, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });

async function generate() {
    hbs.registerPartials('./views/partials', () => {
        fs.readdirSync('./views').forEach(async file => {
            const extension = file.substr(file.length - '.mjml.hbs'.length);
            if (extension !== '.mjml.hbs') {
                return;
            }
            const res = await readFile('./views/' + file);
            const template = hbs.compile(res);
            const context: any = {};
            for (const key of Object.keys(translations['en'])) {
                let translation = '';
                for (const language of languages) {
                    if (!translation) {
                        translation = translation + '*|IF:LANGUAGE=' + language + '|*' + translations[language][key];
                    } else {
                        translation = translation + '*|ELSEIF:LANGUAGE=' + language + '|*' + translations[language][key];
                    }
                }
                translation = translation + '*|ELSE:|*' + translations['en'][key] + '*|END:IF|*';
                context[key] = translation;
            }
            const categories = [];
            for (const group of Object.keys(InterestCategories[site])) {
                // TODO get stuff from database
                categories.push({
                    title: InterestCategories[site][group],
                    slug: group
                });
            }
            context.category = categories;
            const html = template(context);
            console.log(html);
            const parsed = mjml2html(html, {
                beautify: true,
                keepComments: false
            });
            await writeFile('./dist/' + file.split('.')[0] + '.html', parsed.html);
        });
    });
}

generate();

